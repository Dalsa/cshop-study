﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Te
{
    public partial class Form1 : Form
    {
        //전체 객체로 사용하기 위해서 
        Image image;
        Bitmap gBitmap;
        Graphics grBm;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();

            //this.Text = "이미지 테스트";
            //this.Size = new Size(850, 650);
        }

        void setShadowBitmap()
        {
            gBitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            grBm = Graphics.FromImage(gBitmap);
            grBm.Clear(BackColor);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(gBitmap, 0, 0);

            //Graphics G = e.Graphics;
            //G.FillRectangle(new SolidBrush(Color.Red), ClientRectangle);
            //Image bmp = new Bitmap("D:\\te.jpg");
            //G.DrawImage(bmp, 0, 0, 450, 750);
            //G.DrawImage(bmp, ClientRectangle);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Image image;
            grBm = CreateGraphics(); //내부적인 키워드에서 그래픽 영역에서 만들겠다는 기능이 탑재..
            openFileDialog1.Title = "영상 파일 열기";
            openFileDialog1.Filter = "All files(*.*) |*.*| Bitmap File(*.bmp) | Jpg File(*.jpg)";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) //다이얼로그 OK나올시
            {
                string strFilename = openFileDialog1.FileName; //선택된 File 이름을 불러오는 메소드로 string 넣어주기
                image = Image.FromFile(strFilename); //file을 읽어와라.. (선택된 파일을..)
                grBm.DrawImage(image, 10, 50, image.Width, image.Height);
                grBm.Dispose();
            }
        }

        private void 그레이변환ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y, brightness;
            Color color, gray; //color는 클래스가 아니고 구조체이다..
            Graphics gr = CreateGraphics();
            //Bitmap gBitmap = new Bitmap(image);
            
            for(x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = gBitmap.GetPixel(x, y);
                    brightness = (int)(0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                    gray = Color.FromArgb(brightness, brightness, brightness);
                    gBitmap.SetPixel(x, y, gray);
                }
            gr.DrawImage(gBitmap, 40 + gBitmap.Width, 80, gBitmap.Width, gBitmap.Height);
            gr.Dispose();
        }

        private void 밝게전ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y; //z를 왜 넣었었는지 물어보자.
            int r, g, b; //rgb 각각 수를 넣기 위한 변수
            const int ADD_VALUE = 50; //원래는 유동적으로 바꿀 수 있도록 하지만 임의로 50이라는 고정 값을 주었다. (참고로 밝기를 추가할 수)
            Color color;
            Graphics gr = CreateGraphics(); //그래픽 객체를 만든다..?
            //Bitmap gBitmap = new Bitmap(image);

            for(y = 0; y < image.Height; y++)
                for(x = 0; x<image.Width; x++)
                {
                    color = gBitmap.GetPixel(x, y);
                    r = color.R + ADD_VALUE; if (r > 255) r = 255;
                    g = color.G + ADD_VALUE; if (g > 255) g = 255;
                    b = color.B + ADD_VALUE; if (b > 255) b = 255;
                    color = Color.FromArgb(r, g, b);
                    gBitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(gBitmap, 40 + gBitmap.Width, 80, gBitmap.Width, gBitmap.Height);
            gr.Dispose();
            
        }

        private void 차원그레이변환ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int brightness;
            const int ADD_VALUE = 50;
            Color color;
            int[,] grayArray = new int[image.Height, image.Width];
            Graphics gr = CreateGraphics();
            //Bitmap gBitmap = new Bitmap(image);

            for(y = 0; y < image.Height; y++) //이미지를 전체 회색톤으로 바꾸기
                for(x = 0; x < image.Width; x++)
                {
                    color = gBitmap.GetPixel(x, y);
                    brightness = (int)(0.229 * color.R + 0.587 * color.G + 0.114 + color.B);
                    grayArray[y, x] = brightness;
                }

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                {
                    grayArray[y, x] += ADD_VALUE;
                    if (grayArray[y, x] > 255) grayArray[y, x] = 255;
                }

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    gBitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(gBitmap, 40 + gBitmap.Width, 80, gBitmap.Width, gBitmap.Height);
            gr.Dispose();
        }

        private void 컬러영상밝기변환ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y, z;
            const int ADD_VALUE = 30;
            Color color;
            int[,,] colorArray = new int[image.Height, image.Width, 3];
            Graphics gr = CreateGraphics();
            Bitmap gBitmap = new Bitmap(image);

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                {
                    color = gBitmap.GetPixel(x, y);
                    colorArray[y, x, 0] = color.R;
                    colorArray[y, x, 1] = color.G;
                    colorArray[y, x, 2] = color.B;
                }

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                    for(z = 0; z < 3;  z++)
                    {
                        colorArray[y, x, z] += ADD_VALUE;
                        if (colorArray[y, x, z] > 255) colorArray[y, x, z] = 255;
                    }

            for (y = 0; y < 5; y++)
                for (x = 0; x < image.Width; x++)
                    for (z = 0; z < 3; z++)
                    {
                        colorArray[y, x, z] = 0;
                        colorArray[image.Height - 1 - y,x, z] = 0;
                    }

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < 5; x++)
                    for (z = 0; z < 3; z++)
                    {
                        colorArray[y, x, z] = 0;
                        colorArray[y, image.Width - 1 - x, z] = 0;
                    }

            for (y = 0; y < image.Height; y++)
                for(x = 0; x<image.Width; x++)
                {
                    color = Color.FromArgb(colorArray[y, x, 0], colorArray[y, x, 1], colorArray[y, x, 2]);
                    gBitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(gBitmap, 40 + gBitmap.Width, 80, gBitmap.Width, gBitmap.Height);
            gr.Dispose();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save image as ...";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All Files(*.*) | *.*| Bitmap Files(*.bmp) | *.bmp | Jpeg Files(*.jpg) | *.jpg";
            
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                gBitmap.Save(strLowerFilename);
            }
        }

        private void EndToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //2019-09-17 말고 다음 날 저장하고 다양한 코딩방법으로 하는 방법을 설명하준다.
        //10월달 부터 영상처리 기법을 설명한다.


    }
}
