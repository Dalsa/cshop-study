﻿namespace Te
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.endToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.영상처리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그레이변환ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.밝게전ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.차원그레이변환ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.컬러영상밝기변환ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.영상처리ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.endToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // endToolStripMenuItem
            // 
            this.endToolStripMenuItem.Name = "endToolStripMenuItem";
            this.endToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.endToolStripMenuItem.Text = "End";
            this.endToolStripMenuItem.Click += new System.EventHandler(this.EndToolStripMenuItem_Click);
            // 
            // 영상처리ToolStripMenuItem
            // 
            this.영상처리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.그레이변환ToolStripMenuItem,
            this.밝게전ToolStripMenuItem,
            this.차원그레이변환ToolStripMenuItem,
            this.컬러영상밝기변환ToolStripMenuItem});
            this.영상처리ToolStripMenuItem.Name = "영상처리ToolStripMenuItem";
            this.영상처리ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.영상처리ToolStripMenuItem.Text = "영상처리";
            // 
            // 그레이변환ToolStripMenuItem
            // 
            this.그레이변환ToolStripMenuItem.Name = "그레이변환ToolStripMenuItem";
            this.그레이변환ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.그레이변환ToolStripMenuItem.Text = "그레이 변환";
            this.그레이변환ToolStripMenuItem.Click += new System.EventHandler(this.그레이변환ToolStripMenuItem_Click);
            // 
            // 밝게전ToolStripMenuItem
            // 
            this.밝게전ToolStripMenuItem.Name = "밝게전ToolStripMenuItem";
            this.밝게전ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.밝게전ToolStripMenuItem.Text = "밝게전";
            this.밝게전ToolStripMenuItem.Click += new System.EventHandler(this.밝게전ToolStripMenuItem_Click);
            // 
            // 차원그레이변환ToolStripMenuItem
            // 
            this.차원그레이변환ToolStripMenuItem.Name = "차원그레이변환ToolStripMenuItem";
            this.차원그레이변환ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.차원그레이변환ToolStripMenuItem.Text = "2차원그레이변환";
            this.차원그레이변환ToolStripMenuItem.Click += new System.EventHandler(this.차원그레이변환ToolStripMenuItem_Click);
            // 
            // 컬러영상밝기변환ToolStripMenuItem
            // 
            this.컬러영상밝기변환ToolStripMenuItem.Name = "컬러영상밝기변환ToolStripMenuItem";
            this.컬러영상밝기변환ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.컬러영상밝기변환ToolStripMenuItem.Text = "컬러영상밝기변환";
            this.컬러영상밝기변환ToolStripMenuItem.Click += new System.EventHandler(this.컬러영상밝기변환ToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem endToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 영상처리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그레이변환ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 밝게전ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 차원그레이변환ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 컬러영상밝기변환ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

