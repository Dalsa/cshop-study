﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lookups
{
    public partial class Form1 : Form
    {
        //전체 객체로 사용하기 위해서 
        Image image;
        Bitmap bitmap;
        Graphics grBm;
        int[,] grayArray;

        public Form1()
        {
            InitializeComponent(); //기본 초기화 설정 (이미지 처리 하기 위한 필수 메소드)
            setShadowBitmap(); //shadowbitmap
        }

        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height); //클라이언트 사이즈로 비트맵 객체에 새로 넣어줌
            grBm = Graphics.FromImage(bitmap); //그래픽 객체에 이미지 넣어줌
            grBm.Clear(BackColor); //컨트롤의 배경색 없앰
        }

        void copyBitmap2Array() //영상을 회색으로 흑백을 바꾸어 주는 기능
        {
            int x, y; //좌표 변수 
            int brightness; //밝기
            //const int ADD_VALUE = 50;
            Color color;
            grayArray = new int[image.Height, image.Width];
            Graphics gr = CreateGraphics();
            //Bitmap gBitmap = new Bitma

            bitmap = new Bitmap(image);

            for (y = 0; y < image.Height; y++)
            {
                for (x = 0; x < image.Width; x++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                    grayArray[y, x] = brightness;
                }
            }

            //for (y = 0; y < image.Height; y++)
            //{
            //    for (x = 0; x < image.Width; x++)
            //    {
            //        grayArray[y, x] += ADD_VALUE;
            //        if (grayArray[y, x] > 255) grayArray[y, x] = 255;
            //    }
            //}

            for (y = 0; y < image.Height; y++)
            {
                for (x = 0; x < image.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }
            }
            this.ClientSize = new System.Drawing.Size(image.Width, image.Height); //클라이언트 사이즈 변화
            gr.DrawImage(bitmap,0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();
        }

        void displayArray() //폼 화면에 뛰어주는 기능
        {
            int x, y;
            Color color;
            bitmap = new Bitmap(image.Width, image.Height);

            for (y = 0; y < bitmap.Height; y++)
                for(x = 0; x < bitmap.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y,x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }

            grBm.DrawImage(bitmap, 0, 0, image.Width, image.Height);
        }

        private void 빼기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = index - Value;
                if (newValue > 255) newValue = 255;
                LUT[index] = newValue;
            }

            for (y=0; y<bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            displayArray();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Image image;
            grBm = CreateGraphics(); //내부적인 키워드에서 그래픽 영역에서 만들겠다는 기능이 탑재..
            openFileDialog1.Title = "영상 파일 열기";
            openFileDialog1.Filter = "All files(*.*) |*.*| Bitmap File(*.bmp) | Jpg File(*.jpg)";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) //다이얼로그 OK나올시
            {
                string strFilename = openFileDialog1.FileName; //선택된 File 이름을 불러오는 메소드로 string 넣어주기
                image = Image.FromFile(strFilename); //file을 읽어와라.. (선택된 파일을..)
                bitmap = new Bitmap(image);
                copyBitmap2Array(); //이미지 회색 변신
            }
            Invalidate();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save image as ...";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All Files(*.*) | *.*| Bitmap Files(*.bmp) | *.bmp | Jpeg Files(*.jpg) | *.jpg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }

        private void ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = index + Value;
                if (newValue > 255) newValue = 255;
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            displayArray();
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = (int)(index * Value);
                if (newValue > 255) newValue = 255;
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            displayArray();
        }

        private void 나누기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = (int)(index / Value);
                if (newValue > 255) newValue = 255;
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            displayArray();
        }
    }
}
