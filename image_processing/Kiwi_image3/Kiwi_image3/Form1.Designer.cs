﻿namespace Kiwi_image3
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.endToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.blurringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpeningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.sobelmaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobelverticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.라플라시안마스크ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.라플라시안마스크ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.수무딩라프라시안마스크ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.미러링ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수평미러링ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.수직미러링ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.라플라시안마스크ToolStripMenuItem,
            this.미러링ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(365, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.endToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // endToolStripMenuItem
            // 
            this.endToolStripMenuItem.Name = "endToolStripMenuItem";
            this.endToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.endToolStripMenuItem.Text = "End";
            this.endToolStripMenuItem.Click += new System.EventHandler(this.endToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.convolutionToolStripMenuItem,
            this.toolStripMenuItem2,
            this.blurringToolStripMenuItem,
            this.sharpeningToolStripMenuItem,
            this.toolStripMenuItem3,
            this.sobelmaskToolStripMenuItem,
            this.sobelverticalToolStripMenuItem,
            this.toolStripMenuItem4});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "image";
            // 
            // convolutionToolStripMenuItem
            // 
            this.convolutionToolStripMenuItem.Name = "convolutionToolStripMenuItem";
            this.convolutionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.convolutionToolStripMenuItem.Text = "convolution";
            this.convolutionToolStripMenuItem.Click += new System.EventHandler(this.convolutionToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
            // 
            // blurringToolStripMenuItem
            // 
            this.blurringToolStripMenuItem.Name = "blurringToolStripMenuItem";
            this.blurringToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.blurringToolStripMenuItem.Text = "Blurring";
            this.blurringToolStripMenuItem.Click += new System.EventHandler(this.blurringToolStripMenuItem_Click);
            // 
            // sharpeningToolStripMenuItem
            // 
            this.sharpeningToolStripMenuItem.Name = "sharpeningToolStripMenuItem";
            this.sharpeningToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sharpeningToolStripMenuItem.Text = "Sharpening";
            this.sharpeningToolStripMenuItem.Click += new System.EventHandler(this.sharpeningToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(177, 6);
            // 
            // sobelmaskToolStripMenuItem
            // 
            this.sobelmaskToolStripMenuItem.Name = "sobelmaskToolStripMenuItem";
            this.sobelmaskToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sobelmaskToolStripMenuItem.Text = "sobel_horizon";
            this.sobelmaskToolStripMenuItem.Click += new System.EventHandler(this.sobelmaskToolStripMenuItem_Click);
            // 
            // sobelverticalToolStripMenuItem
            // 
            this.sobelverticalToolStripMenuItem.Name = "sobelverticalToolStripMenuItem";
            this.sobelverticalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sobelverticalToolStripMenuItem.Text = "sobel_vertical";
            this.sobelverticalToolStripMenuItem.Click += new System.EventHandler(this.sobelverticalToolStripMenuItem_Click);
            // 
            // 라플라시안마스크ToolStripMenuItem
            // 
            this.라플라시안마스크ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.라플라시안마스크ToolStripMenuItem1,
            this.수무딩라프라시안마스크ToolStripMenuItem});
            this.라플라시안마스크ToolStripMenuItem.Name = "라플라시안마스크ToolStripMenuItem";
            this.라플라시안마스크ToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.라플라시안마스크ToolStripMenuItem.Text = "라플라시안마스크";
            // 
            // 라플라시안마스크ToolStripMenuItem1
            // 
            this.라플라시안마스크ToolStripMenuItem1.Name = "라플라시안마스크ToolStripMenuItem1";
            this.라플라시안마스크ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.라플라시안마스크ToolStripMenuItem1.Text = "라플라시안마스크";
            this.라플라시안마스크ToolStripMenuItem1.Click += new System.EventHandler(this.라플라시안마스크ToolStripMenuItem1_Click);
            // 
            // 수무딩라프라시안마스크ToolStripMenuItem
            // 
            this.수무딩라프라시안마스크ToolStripMenuItem.Name = "수무딩라프라시안마스크ToolStripMenuItem";
            this.수무딩라프라시안마스크ToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.수무딩라프라시안마스크ToolStripMenuItem.Text = "수무딩라프라시안마스크";
            this.수무딩라프라시안마스크ToolStripMenuItem.Click += new System.EventHandler(this.수무딩라프라시안마스크ToolStripMenuItem_Click);
            // 
            // 미러링ToolStripMenuItem
            // 
            this.미러링ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.수평미러링ToolStripMenuItem,
            this.수직미러링ToolStripMenuItem});
            this.미러링ToolStripMenuItem.Name = "미러링ToolStripMenuItem";
            this.미러링ToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.미러링ToolStripMenuItem.Text = "미러링";
            // 
            // 수평미러링ToolStripMenuItem
            // 
            this.수평미러링ToolStripMenuItem.Name = "수평미러링ToolStripMenuItem";
            this.수평미러링ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.수평미러링ToolStripMenuItem.Text = "수평미러링";
            this.수평미러링ToolStripMenuItem.Click += new System.EventHandler(this.수평미러링ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(177, 6);
            // 
            // 수직미러링ToolStripMenuItem
            // 
            this.수직미러링ToolStripMenuItem.Name = "수직미러링ToolStripMenuItem";
            this.수직미러링ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.수직미러링ToolStripMenuItem.Text = "수직미러링";
            this.수직미러링ToolStripMenuItem.Click += new System.EventHandler(this.수직미러링ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 363);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem blurringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpeningToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem sobelmaskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobelverticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 라플라시안마스크ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 라플라시안마스크ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 수무딩라프라시안마스크ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 미러링ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수평미러링ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem 수직미러링ToolStripMenuItem;
    }
}

