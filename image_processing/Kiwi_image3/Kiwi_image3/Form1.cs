﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kiwi_image3
{
    public partial class Form1 : Form
    {
        Graphics gr;
        Image image;
        Bitmap bitmap;
        int[,] grayArray;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();
        }

        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        void copyBitmapArray()
        {
            int x, y, brightness;
            Color color;
            grayArray = new int[bitmap.Height, bitmap.Width];

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(color.R + color.G + color.B);
                    grayArray[y, x] = brightness;
                }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "열어라 이미지 !";
            openFileDialog1.Filter = "All File(*.*) |*.*| Bitmap File(*.bmp) |*.bmp| Jpeg File(*.jpg) |*.jpg";
            
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                this.ClientSize = new System.Drawing.Size(image.Width, image.Height);
                setShadowBitmap();
                gr.DrawImage(image, 0, 30, image.Width, image.Height);
                copyBitmapArray();
            }

            Invalidate();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "이미지를 위기에서 구하기";
            saveFileDialog1.Filter = "All File(*.*) |*.*| Bitmap File(*.bmp) |*.bmp| Jpeg File(*.jpg) |*.jpg";
            
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        int[,] convolve(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow, int biasValue)
        {
            int[,] R = new int[Height, Width];
            int x, y, r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;

                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                            sum += G[y + r, x + c] * M[r, c];

                    sum += biasValue;
                    if (sum > 255.0) sum = 255.0;
                    if (sum < 0.0) sum = 0.0;

                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y,x] =  R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }

            return R;
        }

        void displayResultArray(int[,] resultArray, int Width, int Height)
        {
            int x, y;
            Color color;
            Bitmap bitmap = new Bitmap(Width, Height);

            for (y = 0; y < Height; y++)
                for (x = 0; x < Width; x++)
                {
                    color = Color.FromArgb(resultArray[y, x], resultArray[y, x], resultArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            Invalidate();
        }

        private void convolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { 0.0, -1.0, 0.0 }, { -1.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0 } };
            int[,] ResultArray = convolve(grayArray, image.Width, image.Height, mask, 3, 3, 128);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void endToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void blurringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double mVal = 1.0 / 25.0;
            double[,] mask = { { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal } };
            int[,] ResultArray = convolve(grayArray, image.Width, image.Height, mask, 5, 5, 0);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void sharpeningToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double mVal = 1.0 / 25.0;
            double[,] mask = { { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal } };
            int[,] ResultArray = convolve(grayArray, image.Width, image.Height, mask, 5, 5, 0);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        int[,] convolve2(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow)
        {
            int[,] R = new int[Height, Width];
            int x, y, r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                            sum += G[y + r, x + c] * M[r, c];

                    sum = Math.Abs(sum);

                    if (sum > 255.0) sum = 255.0;
                    if (sum < 0.0) sum = 0.0;
                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y, x] = R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad]; 
                }

            return R;
        }

        private void sobelmaskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { -1.0, 0.0, 1.0 }, { -2.0, 0.0, 2.0 }, { -1.0, 0.0, 1.0 } };
            int[,] ResultArray = convolve2(grayArray, image.Width, image.Height, mask, 3, 3);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void sobelverticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { -1.0, -2.0, -1.0 }, { 0.0, 0.0, 0.0 }, { 1.0, 2.0, 1.0 } };
            int[,] ResultArray = convolve2(grayArray, image.Width, image.Height, mask, 3, 3);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void 라플라시안마스크ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            double[,] mask = { { -1.0, -1.0, -1.0 }, { -1.0, 8.0, -1.0 }, { -1.0, -1.0, -1.0 } };
            int[,] ResultArray = convolve2(grayArray, image.Width, image.Height, mask, 3, 3);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        int[,] convolveNoBias(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow)
        {
            int[,] R = new int[Height, Width];
            int x, y;
            int r, c;
            int xPad = maskCol / 2;
           int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                        {
                            sum += G[y + r, x + c] * M[r, c];
                        }
                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y, x] = R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }

            return R;
        }

        int[,] convolveLp(int[,] G, int Width, int Height, int maskCol, int maskRow)
        {
            int[,] R = new int[Height, Width];
            int x, y;
            int r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            int[] target = new int[maskRow * maskCol];

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    int index = 0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                        {
                            target[index++] = G[y + r, x + c];
                        }R[y + yPad, x + xPad] = zeroCrossing(target, index);
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y, x] = R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }

            return R;
        }

        int zeroCrossing(int[] target, int tsize)
        {
            int mid = tsize / 2;
            if (target[mid] * target[1] < 0 || target[mid] * target[mid - 1] < 0)
            {
                return 255;
            }
            return 0;
        }

        private void 수무딩라프라시안마스크ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { -1.0, -1.0, -1.0 }, { -1.0, 8.0, -1.0 }, { -1.0, -1.0, -1.0 } };

            double[,] gmaks = { { 2.0/115.0, 4.0/115.0 , 5.0/115.0, 4.0/115.0, 2.0/115.0},
                                {4.0/115.0, 9.0/115.0, 12.0/115.0, 9.0/115.0, 4.0/115.0 },
                                { 5.0/115.0, 12.0/115.0, 15.0/115.0, 12.0/115.0, 5.0/115.0 },
                                {4.0/115.0, 9.0/115.0, 12.0/115.0, 9.0/115.0, 4.0/115.0 },
                                {2.0/115.0, 4.0/115.0, 5.0/115.0, 4.0/115.0, 2.0/115.0 } };

            int[,] blurArray = convolve2(grayArray, image.Width, image.Height, gmaks, 5, 5);
            int[,] LaplacianArray = convolveNoBias(blurArray, image.Width, image.Height, mask, 3, 3);
            int[,] ResultArray = convolveLp(LaplacianArray, image.Width, image.Height, 3, 3);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void 수평미러링ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int[,] ResultArray = mirrorH(grayArray, image.Width, image.Height);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        int[,] mirrorH(int[,] G, int Width, int Height)
        {
            int[,] R = new int[Height, Width];
            int x, y;
            for (y = 0; y < Height; y++)
                for (x = 0; x < Width; x++)
                    R[y, x] = G[y, Width - 1 - x];

            return R;
        }

        private void 수직미러링ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int[,] ResultArray = mirrorV(grayArray, image.Width, image.Height);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        int[,] mirrorV(int[,] G, int Width, int Height)
        {
            int[,] R = new int[Height, Width];
            int x, y;
            for (y = 0; y < Height; y++)
                for (x = 0; x < Width; x++)
                {
                    R[y, x] = G[Height - 1 - y, x];
                }

            return R;
        }
    }
}
