﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ninetwofour
{
    public partial class Form1 : Form
    {
        //사용하는 것을 전체 객체로 사용하기
        Image image;
        Bitmap gBitmap;
        Graphics grBm;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();
        }

        void setShadowBitmap()
        {
            gBitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            grBm = Graphics.FromImage(gBitmap);
            grBm.Clear(BackColor);
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e) //파일 오픈
        {
            openFileDialog1.Title = "영상 파일 열기";
            openFileDialog1.Filter = "All files(*.*) |*.*| Bitmap File(*.bmp) |*.bmp| Jpg File(*.jpg)|*.jpg";
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                this.ClientSize = new System.Drawing.Size(image.Width, image.Height);
                setShadowBitmap();
                grBm.DrawImage(image, 0, 0, image.Width, image.Height);
            }
            Invalidate();
        }

        private void GrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int brightness;
            Color color;
            int[,] grayArray = new int[image.Height, image.Width];
            setShadowBitmap();
            Bitmap bit = new Bitmap(image.Width, image.Height);

            for (y = 0; y < image.Height; y++)
                for(x = 0; x <image.Width; x++)
                {
                    color = gBitmap.GetPixel(x, y);
                    brightness = (int)(0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                    grayArray[y, x] = brightness;
                }
            
            for (y = 0; y<image.Height; y++)
                for(x = 0; x<image.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bit.SetPixel(x, y, color);
                }

            this.ClientSize = new System.Drawing.Size(image.Width + image.Width, image.Height);
            grBm.DrawImage(bit, 40 + bit.Width, 0, bit.Width, bit.Height);
            grBm.Dispose();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "저장 하라!!";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All files(*.*) |*.*| Bitmap File(*.bmp) |*.bmp| Jpg File(*.jpg) |*.jpg";
            
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                gBitmap.Save(strLowerFilename);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(gBitmap, 0, 0);
        }

        private void EndToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
