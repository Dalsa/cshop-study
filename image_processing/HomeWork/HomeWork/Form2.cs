﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    public partial class Form2 : Form
    {
        Bitmap bitmap;
        Graphics gr;

        public Form2()
        {
            InitializeComponent();
            setShadowBitmaps();
        }

        void setShadowBitmaps()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        public void showHistogram(Bitmap histobitmap)
        {
            Graphics gr = CreateGraphics();
            bitmap = histobitmap;
            this.ClientSize = new System.Drawing.Size(bitmap.Width, bitmap.Height);
            gr.DrawImage(histobitmap, 0, 0, histobitmap.Width, histobitmap.Height);
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap,0,0);
        }
    }
}
