﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    public partial class Form1 : Form
    {
        Image image;
        Bitmap bitmap;
        Graphics gr;

        int[,] grayArray;
        const int HISTO_WIDTH = 256;
        const int HISTO_HEIGHT = 256;

        Point LastPoint;
        double ratio = 1.0F;
        Point imgPoint;
        Rectangle imgRect;
        Point clickPoint;
        
        public Form1()
        {
            InitializeComponent();

            pictureBox1.MouseWheel += new MouseEventHandler(pictureBox1_MouseWheel);
        }

        void pictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            int lines = e.Delta * SystemInformation.MouseWheelScrollLines / 120;
            PictureBox pb = (PictureBox)sender;

            if(lines > 0)
            {
                ratio *= 1.1F;
                if (ratio > 100.0) ratio = 100.0;
            }
            else if(lines < 0)
            {
                ratio *= 0.9F;
                if (ratio < 1) ratio = 1;
            }

            imgRect.Width = (int)Math.Round(pictureBox1.Width * ratio);
            imgRect.Height = (int)Math.Round(pictureBox1.Height * ratio);
            imgRect.X = (int)Math.Round(pb.Width / 2 - imgPoint.X * ratio);
            imgRect.Y = (int)Math.Round(pb.Height / 2 - imgPoint.Y * ratio);

            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if(pictureBox1.Image != null)
            {
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                e.Graphics.DrawImage(image, imgRect);
                pictureBox1.Focus();
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                imgRect.X = imgRect.X + (int)Math.Round((double)(e.X - clickPoint.X) / 5);
                if (imgRect.X >= 0) imgRect.X = 0;
                if (Math.Abs(imgRect.X) >= Math.Abs(imgRect.Width - pictureBox1.Width)) imgRect.X = -(imgRect.Width - pictureBox1.Width);
                imgRect.Y = imgRect.Y + (int)Math.Round((double)(e.Y - clickPoint.Y) / 5);
                if (imgRect.Y >= 0) imgRect.Y = 0;
                if (Math.Abs(imgRect.Y) >= Math.Abs(imgRect.Height - pictureBox1.Height)) imgRect.Y = -(imgRect.Height - pictureBox1.Height);
            }
            else
            {
                LastPoint = e.Location;
            }

            pictureBox1.Invalidate();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                clickPoint = new Point(e.X, e.Y);
            }
            pictureBox1.Invalidate();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graphics gr = CreateGraphics();
            openFileDialog1.Title = "영상 파일 열기";
            openFileDialog1.Filter = "All Files(*.*) ||*.*|| Bitmap File(*.bmp) |*.bmp| Jpeg File(*.jpg) |*.jpg";
            
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                bitmap = new Bitmap(image);
                this.ClientSize = new System.Drawing.Size(image.Width + 20, image.Height + 50);
                pictureBox1.Size = new System.Drawing.Size(image.Width, image.Height);
                pictureBox1.Image = System.Drawing.Image.FromFile(strFilename);

                imgPoint = new Point(pictureBox1.Width / 2, pictureBox1.Height / 2);
                imgRect = new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height);
                ratio = 1.0;
                clickPoint = imgPoint;

                pictureBox1.Invalidate();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "이미지 저장";
            saveFileDialog1.Filter = "All Files(*.*) ||*.*|| Bitmap File(*.bmp) |*.bmp| Jpeg File(*.jpg) |*.jpg";
            saveFileDialog1.OverwritePrompt = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                //bitmap.Save(strLowerFilename);
                image.Save(strLowerFilename);
            }
        }
        private void endToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void copyBitmapArray()
        {
            int x, y;
            int brightness = 50;
            Color color;
            grayArray = new int[bitmap.Height, bitmap.Width];

            for (x = 0; x < image.Width; x++)
            {
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(color.R * 0.299 + color.G * 0.587 + color.B * 0.114); //너의 회색이 보이니 ?

                    grayArray[y, x] = brightness;
                }
            }
        }

        void displayArray(int[,] grayA)
        {
            int x, y;
            Color color;

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                {
                    color = Color.FromArgb(grayA[y, x], grayA[y, x], grayA[y, x]);
                    bitmap.SetPixel(x, y, color);
                }
            image = (Image)bitmap;
            pictureBox1.Image = image;
            pictureBox1.Invalidate();
        }

        void displayAll()
        {
            image = (Image)bitmap;
            pictureBox1.Image = image;
            pictureBox1.Invalidate(); 
        }

        void viewHistogram(int leftTopx, int leftTopy, int[,] histoArray)
        {
            Form2 histo_dlg = new Form2();
            histo_dlg.Owner = this;
            histo_dlg.Show();

            int x, y;
            Color color;
            Bitmap histoBitmap = new Bitmap(HISTO_WIDTH, HISTO_HEIGHT);
            int[] histogram = new int[256];
            gr = CreateGraphics();
            histogram.Initialize();

            for (y = 0; y < image.Height; y++)
                for (x = 00; x < image.Width; x++)
                    histogram[histoArray[y, x]]++;

            int max_cnt = 0;
            for (x = 0; x < HISTO_WIDTH; x++)
                if (histogram[x] > max_cnt) max_cnt = histogram[x];

            for (x = 0; x < HISTO_WIDTH; x++)
                for (y = 0; y < HISTO_HEIGHT; y++)
                {
                    color = Color.FromArgb(125, 125, 125);
                    histoBitmap.SetPixel(x, y, color);
                }

            for (x = 0; x < HISTO_WIDTH; x++)
            {
                double dHeight = (double)histogram[x] * (HISTO_HEIGHT - 1) / (double)max_cnt;

                for (y = 0; y < dHeight; y++)
                {
                    color = Color.FromArgb(0, 0, 0);
                    histoBitmap.SetPixel(x, (HISTO_HEIGHT - 1) - y, color);
                }
            }

            histo_dlg.showHistogram(histoBitmap);
            
        }

        private void grayToolStripMenuItem_Click(object sender, EventArgs e) //그레이 스케일.. 회색으로 바꾸기
        {
            copyBitmapArray();
            displayArray(grayArray);
        }

        private void strachingToolStripMenuItem_Click(object sender, EventArgs e) //히스토그램 평활화 기법
        {
            int x, y;
            int sum;
            double scale;
            int[] histogram = new int[256];
            int[] LUT = new int[256];
            int numberOfPixels;

            histogram.Initialize();

            for (y = 0; y < image.Height; y++)  // 각 픽셀의 종류별 count
                for (x = 0; x < image.Width; x++)
                    histogram[grayArray[y, x]]++;

            numberOfPixels = image.Width * image.Height;
            sum = 0;
            scale = 255.0 / numberOfPixels;
            for (x = 0; x < 256; x++)
            {
                sum += histogram[x];
                LUT[x] = (int)(sum * scale + 0.5);
            }

            /* transfer new image */
            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                    grayArray[y, x] = LUT[grayArray[y, x]];

            displayArray(grayArray);
        }

        int[,] convolve(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow, int biasValue) //회선 .아직 사용하지 않는 기술
        {
            int[,] R = new int[Height, Width];
            int x, y, r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                        {
                            sum += G[y + r, x + c] * M[r, c];
                        }
                    sum += biasValue;
                    if (sum > 255.0) sum = 255.0;
                    if (sum < 0.0) sum = 0.0;
                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y, x] = R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }

            return R;
        }


        
        private void shapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sharpenImage = bitmap.Clone() as Bitmap;

            int width = image.Width;
            int height = image.Height;

            //임의의 샤프닝 0.0 에서 1.0을 준다.
            double strength = 0.5;

            //create sharpening filter.
            const int filterSize = 5;

            var filter = new double[,]
            {
                {-1,-1,-1,-1,-1 },
                {-1, 2, 2, 2, -1 },
                {-1, 2, 16, 2, -1 },
                {-1, 2, 2, 2, -1 },
                {-1, -1, -1, -1, -1 }
            };

            double bias = 1.0 - strength; //0.5는 샤프닝 0.0 에서 1.0으로 수정 가능
            double factor = strength / 16.0; //여기 0.5도 샤프닝

            const int s = filterSize / 2;

            var result = new Color[image.Width, image.Height];

            if(sharpenImage != null)
            {
                System.Drawing.Imaging.BitmapData pbits = sharpenImage.LockBits(new Rectangle(0, 0, width, height),
                    System.Drawing.Imaging.ImageLockMode.ReadWrite,
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                int bytes = pbits.Stride * height;
                var rgbValues = new byte[bytes];

                System.Runtime.InteropServices.Marshal.Copy(pbits.Scan0, rgbValues, 0, bytes);

                int rgb;

                for (int x = s; x < width - s; x++)
                {
                    for (int y = s; y < height - s; y++)
                    {
                        double red = 0.0, green = 0.0, blue = 0.0;

                        for (int filterX = 0; filterX < filterSize; filterX++)
                        {
                            for (int filterY = 0; filterY < filterSize; filterY++)
                            {
                                int imageX = (x - s + filterX + width) % width;
                                int imageY = (y - s + filterY + height) % height;

                                rgb = imageY * pbits.Stride + 3 * imageX;

                                red += rgbValues[rgb + 2] * filter[filterX, filterY];
                                green += rgbValues[rgb + 1] * filter[filterX, filterY];
                                blue += rgbValues[rgb + 0] * filter[filterX, filterY];
                            }

                            rgb = y * pbits.Stride + 3 * x;

                            int r = Math.Min(Math.Max((int)(factor * red + (bias * rgbValues[rgb + 2])), 0), 255);
                            int g = Math.Min(Math.Max((int)(factor * green + (bias * rgbValues[rgb + 1])), 0), 255);
                            int b = Math.Min(Math.Max((int)(factor * red + (bias * rgbValues[rgb + 2])), 0), 255);

                            result[x, y] = Color.FromArgb(r, g, b);
                        }
                    }
                }

                for (int x = s; x < width - s; x++)
                    for (int y = s; y < height - s; y++)
                    {
                        rgb = y * pbits.Stride + 3 * x;

                        rgbValues[rgb + 2] = result[x, y].R;
                        rgbValues[rgb + 1] = result[x, y].G;
                        rgbValues[rgb + 0] = result[x, y].B;
                    }

                System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, pbits.Scan0, bytes);

                sharpenImage.UnlockBits(pbits);
            }

            bitmap = sharpenImage;

            displayAll();
        }

        private void histogamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewHistogram(image.Width + 20, 20, grayArray);
        }
    }
}
