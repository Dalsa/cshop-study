﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Homework_Montagu
{
    public partial class Form1 : Form
    {
        Image image;
        Graphics gr;
        Bitmap bitmap;

        int[,] grayArray;

        //mouse wheel
        Point LastPoint;
        Point imgPoint;
        double ratio = 1.0F;
        Rectangle imgRect;
        Point clickPoint;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();

            //mouse wheel project
        }


        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }

        void copyBitmapArray()
        {
            int x, y;
            int brightness = 50;
            Color color;
            grayArray = new int[bitmap.Height, bitmap.Width];

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)((color.R * 0.299) + (color.G * 0.587) + (color.B * 0.114));
                    grayArray[y, x] = brightness;
                }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graphics gr = CreateGraphics();
            openFileDialog1.Title = "영상파일 열기";
            openFileDialog1.Filter = "All File(*.*) |*.*| bitmap file(*.bmp) |*.bmp| jpeg File(*.jpg) |*.jpg";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                this.ClientSize = new System.Drawing.Size(image.Width + 3, image.Height + 3);
                setShadowBitmap();
                //gr.DrawImage(image, 0, 0, image.Width, image.Height);
                bitmap = new Bitmap(image);

                pictureBox1 = new PictureBox();
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                imgPoint = new Point(pictureBox1.Width / 2, pictureBox1.Height / 2);
                imgRect = new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height);
                pictureBox1.Image = image;
                ratio = 1.0;
                clickPoint = imgPoint;

                pictureBox1.Invalidate();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save Image as ..";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All File(*.*) |*.*| bitmap file(*.bmp) |*.bmp| jpeg File(*.jpg) |*.jpg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        private void endToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y, brightness;
            Color color, gray;
            gr = CreateGraphics();
            bitmap = new Bitmap(image);

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                    gray = Color.FromArgb(brightness, brightness, brightness);
                    bitmap.SetPixel(x, y, gray);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
