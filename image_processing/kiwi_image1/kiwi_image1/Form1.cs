﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kiwi_image1
{
    public partial class Form1 : Form
    {
        //전역 설정
        Image image;
        Graphics gr;
        Bitmap bitmap;
        int[,] grayArray;

        const int HISTO_WIDTH = 256;
        const int HISTO_HEIGHT = 256;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();
        }

        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }

        void displayArray()
        {
            int x, y;
            Color color;
            bitmap = new Bitmap(image.Width, image.Height);
            gr = CreateGraphics();

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }
            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e) //파일 열기 얍얍
        {
            gr = CreateGraphics();
            openFileDialog1.Title = "영상 파일 열기!!";
            openFileDialog1.Filter = "모든 파일(*.*) |*.*| Bitmap 파일(*.bmp) |*.bmp| Jpeg 파일(*.jpg) |*.jpg";

            if (openFileDialog1.ShowDialog() == DialogResult.OK) //OK 버튼 눌렀을때
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                bitmap = new Bitmap(image); //bitmap 객체에 image 파일 넣기
                //setShadowBitmap();
                this.ClientSize = new System.Drawing.Size(image.Width, image.Height); //폼 크기 변환
                gr.DrawImage(image, 0, 0, image.Width, image.Height); //이미지 폼에 출력
            }
            gr.Dispose();
        }

        private void GrayToolStripMenuItem_Click(object sender, EventArgs e) //회색으로 변신 얍얍
        {
            int x, y;
            int brightness;
            Color color, gray;
            gr = CreateGraphics();
            grayArray = new int[image.Height, image.Width]; //그레이 배열을 전체로 만들어놨다.

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.297 * color.R + 0.587 * color.G + 0.115 * color.B); //회색 변신 얍얍 
                    grayArray[y, x] = brightness;
                    gray = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, gray);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e) //영상 저장
        {
            saveFileDialog1.Title = "이미지 저장하기";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "모든 파일(*.*) |*.*| Bitmap 파일(*.bmp) |*.bmp| Jpeg 파일(*.jpg) |*.jpg|";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e) //폼 끄기
        {
            this.Close();
        }

        private void PlusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = index + Value; //임시로 정해놓은 값을 더한다.
                if (newValue > 255) //값이 255를 넘으면 안된다.. 왜냐면 rgb 값은 255까지 니깐.
                {
                    newValue = 255;
                }
                LUT[index] = newValue; //룩업 테이블에 값을 넣어줌 
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }

            displayArray(); //비트맵 객체 있는 이미지를 출력해준다.
        }

        private void MultiplToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const double Value = 1.5;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = (int)(index * Value);
                if (newValue > 255)
                {
                    newValue = 255;
                }
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }

            displayArray();
        }

        private void MinusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = index - Value;
                if (newValue < 0) //0이 내려가면 안된다.
                {
                    newValue = 0;
                }
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }

            displayArray();
        }

        private void DivideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const double Value = 1.5;
            int[] LUT = new int[256];

            for (int index = 0; index < 255; index++)
            {
                int newValue = (int)(index / Value);
                if (newValue < 0)
                {
                    newValue = 0;
                }
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }

            displayArray();
        }

        private void Gamma25ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const double GAMMA = 2.5;
            int[] Lut = new int[256];

            for (int index = 0; index < 256; index++)
            {
                Lut[index] = (int)(255.0 * Math.Pow((double)index / 255.0, 1.0 / GAMMA));
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = Lut[grayArray[y, x]];
                }

            displayArray();
        }

        private void Gamma05ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const double GAMMA = 0.5;
            int[] Lut = new int[256];

            for (int index = 0; index < 256; index++)
            {
                Lut[index] = (int)(255.0 * Math.Pow((double)index / 255.0, 1.0 / GAMMA));
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = Lut[grayArray[y, x]];
                }

            displayArray();
        }

        private void ReverseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int[] Lut = new int[256];

            for (int index = 0; index < 256; index++)
            {
                Lut[index] = 255 - index;
            }

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = Lut[grayArray[y, x]];
                }

            displayArray();
        }

        //히스토그램용 displayArray
        void displayArray_Histogram(int leftTopX, int leftTopY, int[,] gray)
        {
            int x, y;
            Color color;
            bitmap = new Bitmap(image.Width, image.Height);
            gr = CreateGraphics();

            for (y = 0; y < bitmap.Height; y++)
                for (x = 0; x < bitmap.Width; x++)
                {
                    color = Color.FromArgb(gray[y, x], gray[y, x], gray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }
            gr.DrawImage(bitmap, leftTopX, leftTopY, bitmap.Width, bitmap.Height);

        }

        //히스토그램 나오는 메소드 
        void viewHistogram(int leftTopX, int leftTopY, int[,] histoArray)
        {
            int x, y;
            Color color;
            Bitmap histoBitmap = new Bitmap(HISTO_WIDTH, HISTO_HEIGHT);

            int[] histogram = new int[256];
            histogram.Initialize(); //값을 다 0으로 초기화 됩니다.

            for (y = 0; y < image.Height; y++) //각 픽셀의 종류별 count
                for (x = 0; x < image.Width; x++)
                    histogram[histoArray[y, x]]++;

            int max_cnt = 0; //가장 많은 픽셀

            for (x = 0; x < HISTO_WIDTH; x++)
                if (histogram[x] > max_cnt) max_cnt = histogram[x];

            for (x = 0; x < HISTO_HEIGHT; x++) //x 부터 하는건 위에서 아래로 출력해서 히스토그램을 그리기 위함
            {
                double dHeight = (double)histogram[x] * (HISTO_HEIGHT - 1) / (double)max_cnt; //히스토그램의 높이를 구한다.

                for (y = 0; y < (int)dHeight; y++)
                {
                    color = Color.FromArgb(0, 0, 0); //배경의 임의값을 준다.
                    histoBitmap.SetPixel(x, (HISTO_HEIGHT - 1) - y, color);
                }
            }

            gr.DrawImage(histoBitmap, leftTopX, leftTopY);
        }

        private void HistogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //히스토그램이 나올 사이즈를 늘려준다.
            this.ClientSize = new System.Drawing.Size(image.Width + HISTO_WIDTH + 10, (image.Height > HISTO_HEIGHT) ? 2 * image.Height + 10 : 2 * HISTO_HEIGHT + 10);

            viewHistogram(image.Width + 10, 0, grayArray);

        }

        private void fuzzyStretchingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        int[,] convolve(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow, int biasValue)
        {
            int[,] R = new int[Height, Width];
            int x, y, r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                        {
                            sum += G[y + r, x + c] * M[r, c];
                        }

                    sum += biasValue;

                    if (sum > 255.0) sum = 255.0;
                    if (sum < 0.0) sum = 0.0;

                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[Height - 1 - y, x] = R[Height - 1 - yPad, x];
                }

            for (x = 0; x < xPad; x++)
                for (y = 0; y < Height; y++)
                {
                    R[y, x] = R[y, xPad];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }

            return R;
        }

        void displayResultArray(int[,] resultArray, int Width, int Height)
        {
            int x, y;
            Color color;
            Bitmap bitmap = new Bitmap(Width, Height); //임의로 내부에 똑같은 비트맵 생성.
            for (y = 0; y < Height; y++)
                for (x = 0; x < Width; x++)
                {
                    color = Color.FromArgb(resultArray[y, x], resultArray[y, x], resultArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            Invalidate();
        }

        private void convolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { 0.0, -1.0, 0.0 }, { -1.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0 } };
            int[,] ResultArray = convolve(grayArray, image.Width, image.Height, mask, 3, 3, 128);
            displayResultArray(ResultArray, image.Width, image.Height);
        }

        private void blurringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double mVal = 1.0 / 25.0;
            double[,] mask = { { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal }, { mVal, mVal, mVal, mVal, mVal },
            { mVal, mVal, mVal, mVal, mVal },{ mVal, mVal, mVal, mVal, mVal }};
            int[,] ResultArray = convolve(grayArray, image.Width, image.Height, mask, 5, 5, 0);
        }

        int[,] convolve2(int[,] G, int Width, int Height, double[,] M, int maskCol, int maskRow)
        {
            int[,] R = new int[Height, Width];
            int x, y, r, c;
            int xPad = maskCol / 2;
            int yPad = maskRow / 2;
            double sum;

            for (y = 0; y < Height - 2 * yPad; y++)
                for (x = 0; x < Width - 2 * xPad; x++)
                {
                    sum = 0.0;
                    for (r = 0; r < maskRow; r++)
                        for (c = 0; c < maskCol; c++)
                        {
                            sum += G[y + r, x + c] * M[r, c];
                        }
                    sum = Math.Abs(sum);
                    if (sum > 255.0) sum = 255.0;
                    if (sum < 0) sum = 0.0;

                    R[y + yPad, x + xPad] = (int)sum;
                }

            for (y = 0; y < yPad; y++)
            {
                for (x = xPad; x < Width - xPad; x++)
                {
                    R[y, x] = R[yPad, x];
                    R[y, Width - 1 - x] = R[y, Width - 1 - xPad];
                }
            }

            return R;

        }

        private void ftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] mask = { { -1.0, 0.0, 1.0 }, { -2.0, 0, 2.0 }, { -1.0, 0.0, 1.0 } };
        }
    }
}
