﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fuzzy
{
    public partial class Form1 : Form
    {
        Image image;
        Graphics gr;
        Bitmap bitmap;

        int[,] grayArray;
        const int HISTO_WIDTH = 256;
        const int HISTO_HEIGHT = 250;

        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();
        }

        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        private void menuStrip1_Paint(object sender, PaintEventArgs e)
        {
            
        } //실수로 만들었따.

        void copyBitmapArray()
        {
            int x, y;
            int brightness = 50;
            Color color;
            grayArray = new int[bitmap.Height, bitmap.Width];

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(color.R + color.G + color.G) / 3;
                    grayArray[y, x] = brightness;
                }
        }

        void viewHistogram(int leftTopx, int leftTopy, int[,] histoArray)
        {
            int x, y;
            Color color;
            Bitmap histoBitmap = new Bitmap(HISTO_WIDTH, HISTO_HEIGHT);
            int[] histogram = new int[256];
            histogram.Initialize();

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                    histogram[histoArray[y,x]]++;
            int max_cnt = 0;

            for (x = 0; x < HISTO_WIDTH; x++)
                if (histogram[x] > max_cnt) max_cnt = histogram[x];

            for (x = 0; x < HISTO_WIDTH; x++)
                for (y = 0; y < HISTO_HEIGHT; y++)
                {
                    color = Color.FromArgb(125, 125, 125);
                    histoBitmap.SetPixel(x, y, color);
                }

            for (x = 0; x < HISTO_WIDTH; x++)
            {
                double dHeight = (double)histogram[x] * (HISTO_HEIGHT - 1) / (double)max_cnt;

                for (y = 0; y < dHeight; y++)
                {
                    color = Color.FromArgb(0, 0, 0);
                    histoBitmap.SetPixel(x, (HISTO_HEIGHT - 1) - y, color);
                }
            }

            gr.DrawImage(histoBitmap, leftTopx, leftTopy);
            Invalidate();
        }

        void displayArray(int leftTopx, int leftTopy, int[,] grayA)
        {
            int x, y;
            Color color;
            Bitmap bitmap = new Bitmap(image.Width, image.Height);

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                {
                    color = Color.FromArgb(grayA[y, x], grayA[y, x], grayA[y, x]);
                    bitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(bitmap, leftTopx, leftTopy, image.Width, image.Height);
            Invalidate();
        }

        private void fuzzyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int Xmean, Xmin = 255, Xmax = 0, Dmin, Dmax, a, Imax, Imin, Imid, x, y, r = 0, g = 0, b = 0;
            double l_value, r_value, percent = 0.05;
            Color color;

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    r += color.R;
                    g += color.G;
                    b += color.B;

                    if (Xmin > (color.R + color.G + color.B) / 3)
                        Xmin = (color.R + color.G + color.B) / 3;
                    if (Xmax < (color.R + color.G + color.B) / 3)
                        Xmax = (color.R + color.G + color.B) / 3;
                }

            r = r / (image.Height * image.Width);
            g = g / (image.Height * image.Width);
            b = b / (image.Height * image.Width);
            Xmean = (r + g + b) / 3;

            Dmax = Xmax - Xmean;
            Dmin = Xmean - Xmin;

            if (Xmean > 128)
                a = 255 - Xmean;
            else if (Xmean <= Dmin)
                a = Dmin;
            else if (Xmean >= Dmax)
                a = Dmax;
            else
                a = Xmean;

            Imax = Xmean + a;
            Imin = Xmean - a;
            Imid = (Imax + Imin) / 2;

            if (Imin != 0) percent = (double)Imin / (double)Imax;
            l_value = (Imid - Imin) * percent + Imin;
            r_value = -(Imax - Imid) * percent + Imin;

            int alpha = (int)l_value;
            int beta = (int)r_value;
            int[] histogram = new int[256];
            int[] LUT = new int[256];

            histogram.Initialize();

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                    histogram[grayArray[y, x]]++;

            for (x = 0; x < alpha; x++)
                LUT[x] = 0;

            for (x = 255; x > beta; x--)
                LUT[x] = 255;

            for (x = alpha; x <= beta; x++)
                LUT[x] = (int)((x - alpha) * 255.0 / (beta - alpha));

            for (y = 0; y < image.Height; y++)
                for (x = 0; x < image.Width; x++)
                    grayArray[y, x] = LUT[grayArray[y, x]];

            displayArray(0, image.Height + 10, grayArray);

            viewHistogram(image.Width + 10, 256, grayArray);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graphics gr = CreateGraphics();
            openFileDialog1.Title = "영상파일열기";
            openFileDialog1.Filter = "All Files(*.*) |*.*| Bitmap File (*.bmp) | *.bmp | Jpeg File (*.jpg) | *.jpg";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);

                this.ClientSize = new System.Drawing.Size(image.Width + 280 * 3, 4 * image.Height);

                setShadowBitmap();
                gr.DrawImage(image, 0, 0, image.Width, image.Height);
                copyBitmapArray();
                viewHistogram(image.Width + 10, 0, grayArray);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save Image as..";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All Files(*.*) |*.*| Bitmap File (*.bmp) | *.bmp | Jpeg File (*.jpg) | *.jpg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        private void endToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }
    }
}
