﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Image image;
        Graphics gr;
        Bitmap bitmap;

        int[,] grayArray;
        public Form1()
        {
            InitializeComponent();
        }

        
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filterName = "All Files(*.*) |*.*| Bitmap file(*.bmp) | *.bmp | Jpeg File(*.jpg) | *.jpg";
            gr = CreateGraphics();
            openFileDialog1.Title = "영상 파일 열기";
            openFileDialog1.Filter = filterName;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                image = Image.FromFile(filename);
                this.Size = new Size(image.Width, image.Height);
                bitmap = new Bitmap(image);
                //gr.DrawImage(image, 0, 0, image.Width, image.Height);
                copyBitmap2Array();
            }
            
        }

        private void copyBitmap2Array()
        {
            int x, y, brightness;
            Color color;
            grayArray = new int[image.Height, image.Width];

            for (y=0; y<image.Height; y++)
            {
                for (x=0; x<image.Width; x++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                    grayArray[y, x] = brightness;
                }
            }
            displayArray();
        }

        private void displayArray()
        {
            int x, y;
            Color color;
            bitmap = new Bitmap(image.Width, image.Height);

            for (y = 0; y < image.Height; y++)
            {
                for (x = 0; x < image.Width; x++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }
            }
            gr.DrawImage(bitmap, 0, 0, image.Width, image.Height);
        }

        private void opToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index=0; index<256; index++)
            {
                int newValue = index + Value;
                if (newValue > 255) newValue = 255;
                LUT[index] = newValue;
            }

            for (y=0; y<bitmap.Height; y++)
            {
                for (x=0; x<bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }
            displayArray();
        }

        private void opToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int x, y;
            const double Value = 1.5;
            int[] LUT = new int[256];

            for (int index = 0; index < 256; index++)
            {
                double newValue = index * Value;
                if (newValue > 255) newValue = 255;
                LUT[index] = (int)newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
            {
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }
            displayArray();
        }

        private void opToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            int x, y;
            const int Value = 35;
            int[] LUT = new int[256];

            for (int index = 0; index < 256; index++)
            {
                int newValue = index - Value;
                if (newValue< 0) newValue = 0;
                LUT[index] = newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
            {
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }
            displayArray();
        }

        private void opToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            int x, y;
            const double Value = 1.5;
            int[] LUT = new int[256];

            for (int index = 0; index < 256; index++)
            {
                double newValue = index / Value;
                if (newValue < 0) newValue = 255;
                LUT[index] = (int)newValue;
            }

            for (y = 0; y < bitmap.Height; y++)
            {
                for (x = 0; x < bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }
            displayArray();
        }

        private void gAMMAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int[] LUT = new int[256];
            const double GAMMA = 2.5;
            for (int index=0; index<256; index++)
            {
                LUT[index] = (int)(255.0 * Math.Pow((double)index / 255.0, 1.0 / GAMMA));
            }

            for (y=0; y<bitmap.Height; y++)
            {
                for (x=0; x<bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }

            displayArray();
        }

        private void inverseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int[] LUT = new int[256];
            for (int index = 0; index < 256; index++)
            {
                LUT[index] = 255 - index;
            }
            for (y=0; y<bitmap.Height; y++)
            {
                for (x=0; x<bitmap.Width; x++)
                {
                    grayArray[y, x] = LUT[grayArray[y, x]];
                }
            }
            displayArray();
        }
    }
}
