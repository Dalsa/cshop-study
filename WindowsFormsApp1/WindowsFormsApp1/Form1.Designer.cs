﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageProcessingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.opToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.opToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.gAMMAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.inverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.imageProcessingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // imageProcessingToolStripMenuItem
            // 
            this.imageProcessingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opToolStripMenuItem,
            this.opToolStripMenuItem1,
            this.opToolStripMenuItem2,
            this.opToolStripMenuItem3,
            this.gAMMAToolStripMenuItem,
            this.inverseToolStripMenuItem});
            this.imageProcessingToolStripMenuItem.Name = "imageProcessingToolStripMenuItem";
            this.imageProcessingToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.imageProcessingToolStripMenuItem.Text = "Image Processing";
            // 
            // opToolStripMenuItem
            // 
            this.opToolStripMenuItem.Name = "opToolStripMenuItem";
            this.opToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.opToolStripMenuItem.Text = "+ op";
            this.opToolStripMenuItem.Click += new System.EventHandler(this.opToolStripMenuItem_Click);
            // 
            // opToolStripMenuItem1
            // 
            this.opToolStripMenuItem1.Name = "opToolStripMenuItem1";
            this.opToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.opToolStripMenuItem1.Text = "* op";
            this.opToolStripMenuItem1.Click += new System.EventHandler(this.opToolStripMenuItem1_Click);
            // 
            // opToolStripMenuItem2
            // 
            this.opToolStripMenuItem2.Name = "opToolStripMenuItem2";
            this.opToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.opToolStripMenuItem2.Text = "- op";
            this.opToolStripMenuItem2.Click += new System.EventHandler(this.opToolStripMenuItem2_Click);
            // 
            // opToolStripMenuItem3
            // 
            this.opToolStripMenuItem3.Name = "opToolStripMenuItem3";
            this.opToolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.opToolStripMenuItem3.Text = "/ op";
            this.opToolStripMenuItem3.Click += new System.EventHandler(this.opToolStripMenuItem3_Click);
            // 
            // gAMMAToolStripMenuItem
            // 
            this.gAMMAToolStripMenuItem.Name = "gAMMAToolStripMenuItem";
            this.gAMMAToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gAMMAToolStripMenuItem.Text = "GAMMA";
            this.gAMMAToolStripMenuItem.Click += new System.EventHandler(this.gAMMAToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // inverseToolStripMenuItem
            // 
            this.inverseToolStripMenuItem.Name = "inverseToolStripMenuItem";
            this.inverseToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.inverseToolStripMenuItem.Text = "Inverse";
            this.inverseToolStripMenuItem.Click += new System.EventHandler(this.inverseToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem imageProcessingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem opToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem opToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem gAMMAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inverseToolStripMenuItem;
    }
}

