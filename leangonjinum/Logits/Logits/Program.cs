﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logits
{
    class Program
    {
        static int[] X = null;
        static double[] W = null;

        static void Main(string[] args)
        {
            bool loopFlag = true;

            while (loopFlag)
            {
                Console.WriteLine("논리 게이트를 선택하세요.");
                Console.WriteLine("[1] AND gate");
                Console.WriteLine("[2] OR gate");
                Console.WriteLine("[3] NOT gate");
                Console.WriteLine("[0] 종료");
                Console.Write("선택 : ");

                int gateNum = Int32.Parse(Console.ReadLine());

                string line = "";
                string[] tokens = null;
                double NET = 0.0;
                int Y = 0;

                switch (gateNum)
                {
                    case 1:
                        //AND gate
                        X = new int[2];
                        W = new double[2];

                        Console.Write("x1, x2를 입력하세요(예시: 1 0 ) : ");
                        line = Console.ReadLine();

                        tokens = line.Split();
                        X[0] = Int32.Parse(tokens[0]);
                        X[1] = Int32.Parse(tokens[1]);

                        W[0] = 0.5;
                        W[1] = 0.5;

                        Console.WriteLine("입력된 X : {0} {1}", X[0], X[1]);
                        Console.WriteLine("조정된 W : {0} {1}", W[0], W[1]);
                        Console.WriteLine("임계치 T : 1");

                        NET = X[0] * W[0] + X[1] * W[1];
                        Y = (NET >= 1) ? 1 : 0;

                        Console.WriteLine("연산 결과 : {0}", Y);
                        break;
                    case 2:
                        //OR gate
                        X = new int[2];
                        W = new double[2];

                        Console.Write("x1, x2를 입력하세요.(예시: 1 0 ): ");

                        line = Console.ReadLine();

                        tokens = line.Split();
                        X[0] = Int32.Parse(tokens[0]);
                        X[1] = Int32.Parse(tokens[1]);


                        W[0] = 1;
                        W[1] = 1;

                        Console.WriteLine("입력된 X : {0} {1}", X[0], X[1]);
                        Console.WriteLine("조정된 W : {0} {1}", W[0], W[1]);
                        Console.WriteLine("임계치 T : 1");

                        NET = X[0] * W[0] + X[1] * W[1];
                        Y = (NET >= 1) ? 1 : 0;

                        Console.WriteLine("연산 결과 : {0}", Y);
                        break;
                    case 3:
                        //not gate
                        X = new int[1];
                        W = new double[1];

                        Console.Write("x1을 입력하세요(예시 : 1) :");
                        line = Console.ReadLine();

                        X[0] = Int32.Parse(line);
                        W[0] = -1;

                        Console.WriteLine("입력된 X: {0}", X[0]);
                        Console.WriteLine("조정된 W: {0}", W[0]);
                        Console.WriteLine("임계치 T : 0");

                        NET = X[0] * W[0];
                        Y = (NET >= 0) ? 1 : 0;

                        Console.WriteLine("연산 결과 : {0}", Y);
                        break;
                    case 0:
                        loopFlag = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
