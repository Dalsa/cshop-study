﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeeDongHyeon201595054
{
    public partial class Form1 : Form
    {
        Image image;
        Graphics gr;
        Bitmap bitmap;
        int[,] grayArray;
        
        public Form1()
        {
            InitializeComponent();
            setShadowBitmap();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImage(bitmap, 0, 0);
        }

        void setShadowBitmap()
        {
            bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            gr = Graphics.FromImage(bitmap);
            gr.Clear(BackColor);
        }

        void copyBitmapArray()
        {
            int x, y;
            int brightness;
            Color color, gray;
            gr = CreateGraphics();
            grayArray = new int[image.Height, image.Width];

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.297 * color.R + 0.587 * color.G + 0.115 * color.B);
                    grayArray[y, x] = brightness;
                    gray = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, gray);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gr = CreateGraphics();
            openFileDialog1.Title = "영상 파일 열기!!";
            openFileDialog1.Filter = "모든 파일(*.*) |*.*| Bitmap 파일(*.bmp) |*.bmp| Jpeg 파일(*.jpg) |*.jpg";

            if (openFileDialog1.ShowDialog() == DialogResult.OK) //OK 버튼 눌렀을때
            {
                string strFilename = openFileDialog1.FileName;
                image = Image.FromFile(strFilename);
                bitmap = new Bitmap(image); //bitmap 객체에 image 파일 넣기
                //setShadowBitmap();
                this.ClientSize = new System.Drawing.Size(image.Width, image.Height); //폼 크기 변환
                gr.DrawImage(image, 0, 0, image.Width, image.Height);
            }
            gr.Dispose();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "이미지 저장";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Filter = "All FIles(*.*) |*.*| jpeg file(*.jpg) |*.jpg| Bitmap File(*.bmp) |*.bmp";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string strFilename = saveFileDialog1.FileName;
                string strLowerFilename = strFilename.ToLower();
                bitmap.Save(strLowerFilename);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            Color color;

            int Pmax = 0, Pmin = 255;
            int T;
            int temp;


            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    //grayarray에 이미 들어있어서 시도함. 
                    temp = grayArray[y,x]; //이럴수가.

                    //큰거 작음거 추출
                    if (Pmax < temp) Pmax = temp;
                    if (Pmin > temp) Pmin = temp;
                }

            T = (Pmax + Pmin) / 2;

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    temp = grayArray[y, x];

                    if(T >= temp)
                    {
                        grayArray[y, x] = 255;
                    }
                    else
                    {
                        grayArray[y, x] = 0;
                    }
                }

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, color);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();

        }

        private void grayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x, y;
            int brightness;
            Color color, gray;
            gr = CreateGraphics();
            grayArray = new int[image.Height, image.Width]; //그레이 배열을 전체로 만들어놨다.

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    color = bitmap.GetPixel(x, y);
                    brightness = (int)(0.297 * color.R + 0.587 * color.G + 0.115 * color.B); //회색 변신 얍얍 
                    grayArray[y, x] = brightness;
                    gray = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, gray);
                }

            int Pmax = 0, Pmin = 255;
            int T;
            int temp;


            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    //grayarray에 이미 들어있어서 시도함. 
                    temp = grayArray[y, x]; //이럴수가.

                    //큰거 작음거 추출
                    if (Pmax < temp) Pmax = temp;
                    if (Pmin > temp) Pmin = temp;
                }

            T = (Pmax + Pmin) / 2;

            for (x = 0; x < image.Width; x++)
                for (y = 0; y < image.Height; y++)
                {
                    temp = grayArray[y, x];

                    if (T >= temp)
                    {
                        grayArray[y, x] = 255;
                    }
                    else
                    {
                        grayArray[y, x] = 0;
                    }

                    gray = Color.FromArgb(grayArray[y, x], grayArray[y, x], grayArray[y, x]);
                    bitmap.SetPixel(x, y, gray);
                }

            gr.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            gr.Dispose();
        }
    }
}
