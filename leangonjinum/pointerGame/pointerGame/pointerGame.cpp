﻿// pointerGame.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <stdio.h>
#pragma warning(disable:4996);

int kgb(int*, int*);
int pb(int**, int**);

int main()
{
    int x, y;
    printf("x,y값 입력 : ");
    scanf("%d %d", &x, &y);
    printf("\n");
    printf("%d + %d = %d\n", x, y, kgb(&x, &y));
    /*static char s1[] = { "kim" }, s2[] = { "lee" }, s3[] = { "cho" };
    static char *name[] = { s1,s2,s3 };
    printf("name[0]의 첫 주소 값은 = %s\n", name[0]);
    printf("name[1]의 첫 주소 값은 = %s\n", name[1]);
    printf("name[2]의 첫 주소 값은 = %s\n", name[2]);

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++) {
            printf("name[%d][%d] == %c\n", i, j, name[i][j]);
        }*/

    /*char k = 'A';
    char *kg, **kgg;
    kg = &k;
    kgg = &kg;
    putchar(k);
    printf("\n");
    putchar(*kg);
    printf("\n");
    putchar(**kgg);
    printf("\n");*/

    /*char kgb[] = { "kim, KB" };
    char* kg, ** kgg;
    kg = kgb;
    kgg = &kg;
    putchar(*(kg + 1));
    printf("\n");
    putchar(*(*(kgg)+1));
    printf("\n");
    puts(kg);
    printf("\n");
    puts(*kgg);*/

    /*char la1[] = { "vc++" }, la2[] = { "vb" }, la3[] = { "Delphi" }, la4[] = { "JAVA" };
    char *lag[] = { la1,la2,la3,la4 };
    char** pa[] = { lag,lag + 1,lag + 2,lag + 3 };
    printf("**(la+1) = %s\n", **(pa + 1));
    printf("**la = %s\n", **pa);
    printf("**(la+1) = %s\n", **(pa + 1));
    printf("**(la+2) = %s\n", **(pa + 2));
    printf("**(la+3) = %s\n", **(pa + 3));*/
}

int kgb(int *a, int *b) {
    return pb( &a, &b);
}

int pb(int **g, int **h) {
    return **g + **h;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
